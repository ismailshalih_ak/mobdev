package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "category_table")
public class Category {

    @PrimaryKey
    @NonNull
    public String name;

    public String color;

    public Category(String name, String color) {
        this.name = name;
        this.color = color;
    }
}
