package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.List;

@Entity(tableName = "task_table", foreignKeys = @ForeignKey(entity = Category.class,
        parentColumns = "name", childColumns = "category_name"))
public class Task implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "task_id")
    public long id;

    public String title;

    public String due;

    @ColumnInfo(name = "is_done")
    public boolean isDone;

    @ColumnInfo(name = "category_name")
    public String categoryName;

    public Task(String title, String due, boolean isDone, String categoryName) {
        this.title = title;
        this.due = due;
        this.isDone = isDone;
        this.categoryName = categoryName;
    }
}