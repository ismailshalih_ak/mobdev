package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Category;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Repository;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Subtask;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Task;

public class AddEditTaskViewModel extends AndroidViewModel {
    private Repository repository;

    public AddEditTaskViewModel(@NonNull Application application){
        super(application);
        repository = new Repository(application);
    }

    public void insert(Task task, List<Subtask> subtasks) {
        long taskId = repository.insert(task);
        if(subtasks != null) {
            for (Subtask subtask: subtasks) {
                subtask.taskId = taskId;
                repository.insert(subtask);
            }
        }
    }

    public void update(Task task, List<Subtask> subtasks) {
        repository.update(task);
        //TODO implement update subtasks of task
    }

    public LiveData<List<String>> getAllCategoryNames() {
        return repository.getCategoryNames();
    }
}