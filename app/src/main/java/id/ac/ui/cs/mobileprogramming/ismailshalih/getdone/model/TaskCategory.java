package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.Relation;

public class TaskCategory {

    @Embedded
    public Task task;

    @Embedded
    public Category category;
}
