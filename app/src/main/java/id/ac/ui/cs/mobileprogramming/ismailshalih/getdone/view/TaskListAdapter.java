package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.view;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.R;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Category;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Task;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.TaskCategory;

public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.TaskHolder> {
    private List<TaskCategory> tasks = new ArrayList<>();
    private Calendar c = Calendar.getInstance();
    private OnItemClickListener listener;

    private static final long MINUTE = 60000;
    private static final long HOUR = MINUTE * 60;
    private static final long DAY = HOUR * 24;

    @NonNull
    @Override
    public TaskHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_item, parent, false);
        return new TaskHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final TaskHolder holder, int position) {
        Task currentTask = tasks.get(position).task;
        holder.textViewTitle.setText(currentTask.title);

        long countDownTime = Long.parseLong(currentTask.due + "000") - c.getTimeInMillis();
        if(countDownTime > 0) {
            new CountDownTimer(countDownTime, 60000) {

                public void onTick(long millisUntilFinished) {
                    if (millisUntilFinished / DAY > 0) holder.textViewCountdown.setText("Due in " + millisUntilFinished / DAY + " days " + (millisUntilFinished % DAY) / HOUR + " hours");
                    else if(millisUntilFinished / HOUR > 0) holder.textViewCountdown.setText("Due in " + millisUntilFinished / HOUR + " hours " + (millisUntilFinished % HOUR) / MINUTE + " minutes");
                    else holder.textViewCountdown.setText("Due in " + millisUntilFinished / MINUTE + " minutes");
                }

                public void onFinish() {
                    holder.textViewCountdown.setText(R.string.overdue);
                    holder.textViewCountdown.setTextColor(Color.parseColor("#FF0000"));
                }
            }.start();
        } else {
            holder.textViewCountdown.setText(R.string.overdue);
            holder.textViewCountdown.setTextColor(Color.parseColor("#FF0000"));
        }

        holder.viewBottomBordor.setBackgroundColor(Color.parseColor(tasks.get(position).category.color));
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public void setTasks(List<TaskCategory> tasks) {
        this.tasks = tasks;
        notifyDataSetChanged();
    }

    class TaskHolder extends RecyclerView.ViewHolder {
        private TextView textViewTitle;
        private TextView textViewCountdown;
        private View viewBottomBordor;

        public TaskHolder(View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewCountdown = itemView.findViewById((R.id.text_view_countdown));
            viewBottomBordor = itemView.findViewById(R.id.view_bottom_border);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if(listener != null && position != RecyclerView.NO_POSITION){

                    }
                    listener.onItemClick(tasks.get(position).task);
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Task task);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }
}
