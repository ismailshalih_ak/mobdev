package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface CategoryDao {

    @Insert
    void insert(Category category);

    @Update
    void update(Category category);

    @Delete
    void delete(Category category);

    @Query("DELETE FROM category_table")
    void deleteAllCategories();

    @Query("SELECT * FROM category_table")
    LiveData<List<Category>> getAllCategories();

    @Query("SELECT * FROM category_table WHERE name = :name")
    Category getCategoryByName(String name);

    @Query("SELECT name FROM category_table ORDER BY name ASC")
    LiveData<List<String>> getAllCategoryNames();
}
