package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.R;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Task;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.viewmodel.AddEditTaskViewModel;

public class AddEditTaskActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private AddEditTaskViewModel addEditTaskViewModel;

    private EditText editTextTitle;
    private TextView textDueDate;
    private TextView textDueTime;
    private Spinner spinnerCategory;
    private Calendar dueTime;
    private List<String> categories;

    private boolean isEdit;
    private Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_task);

        editTextTitle = findViewById(R.id.edit_text_title);
        textDueDate = findViewById(R.id.text_due_date);
        textDueTime = findViewById(R.id.text_due_time);
        spinnerCategory = findViewById(R.id.spinner_category);

        dueTime = Calendar.getInstance();
        Intent intent = getIntent();

        if(intent.hasExtra("Task")) {
            this.task = (Task) intent.getSerializableExtra("Task");
            editTextTitle.setText(this.task.title);
            dueTime.setTimeInMillis(Long.parseLong(this.task.due+"000"));

            this.isEdit = true;
            setTitle("Edit Task");
        } else {
            dueTime.add(Calendar.DAY_OF_YEAR, 1);

            this.isEdit = false;
            setTitle("Add Task");
        }

        addEditTaskViewModel = ViewModelProviders.of(this).get(AddEditTaskViewModel.class);
        addEditTaskViewModel.getAllCategoryNames().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> categories) {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddEditTaskActivity.this, android.R.layout.simple_list_item_1, categories);
                spinnerCategory.setAdapter(adapter);
                if(AddEditTaskActivity.this.task != null) spinnerCategory.setSelection(categories.indexOf(AddEditTaskActivity.this.task.categoryName));
            }
        });

        textDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment(dueTime);
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });

        textDueTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePickerFragment(dueTime);
                timePicker.show(getSupportFragmentManager(), "time picker");
            }
        });

        textDueDate.setText(DateFormat.getDateInstance(DateFormat.FULL).format(dueTime.getTime()));
        textDueTime.setText(DateFormat.getTimeInstance(DateFormat.SHORT).format(dueTime.getTime()));

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_task_menu, menu);
        return true;
    }

    private void saveTask() {
        String title = editTextTitle.getText().toString().trim();
        String due = "" + dueTime.getTimeInMillis()/1000;
        Log.i("Save", title + due);
        if(title.isEmpty()) {
            Toast.makeText(this, "Please insert title", Toast.LENGTH_SHORT).show();
            return;
        }
        if(this.isEdit) {
            this.task.title = title;
            this.task.due = due;
            this.task.categoryName = spinnerCategory.getSelectedItem().toString();
            addEditTaskViewModel.update(this.task, null);
        }
        else addEditTaskViewModel.insert(new Task(title, due, false, spinnerCategory.getSelectedItem().toString()), null);

        setResult(RESULT_OK);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_task:
                saveTask();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        dueTime.set(Calendar.YEAR, year);
        dueTime.set(Calendar.MONTH, month);
        dueTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String dueDateString = DateFormat.getDateInstance(DateFormat.FULL).format(dueTime.getTime());

        textDueDate.setText(dueDateString);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        dueTime.set(Calendar.HOUR, hourOfDay);
        dueTime.set(Calendar.MINUTE, minute);
        String dueTimeString = DateFormat.getTimeInstance(DateFormat.SHORT).format(dueTime.getTime());
        textDueTime.setText(dueTimeString);
    }
}
