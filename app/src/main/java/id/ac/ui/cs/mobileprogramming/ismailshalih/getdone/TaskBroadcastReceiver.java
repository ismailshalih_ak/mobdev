package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Repository;

public class TaskBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Toast.makeText(context, R.string.ready_message, Toast.LENGTH_SHORT).show();
        }
    }
}
