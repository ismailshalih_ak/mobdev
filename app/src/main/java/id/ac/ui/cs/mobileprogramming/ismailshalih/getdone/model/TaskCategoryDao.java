package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

@Dao
public interface TaskCategoryDao {
    @Query("SELECT * FROM task_table T, category_table C WHERE T.category_name = C.name")
    LiveData<List<TaskCategory>> getAllTaskCategory();
}
