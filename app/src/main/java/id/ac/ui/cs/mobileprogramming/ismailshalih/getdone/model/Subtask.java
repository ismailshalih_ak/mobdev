package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "subtask_table", foreignKeys = @ForeignKey(entity = Task.class, parentColumns = "task_id", childColumns = "task_id", onDelete = ForeignKey.CASCADE))
public class Subtask {

    @PrimaryKey(autoGenerate = true)
    public long id;

    @ColumnInfo(name = "subtask_title")
    public String subtaskTitle;

    @ColumnInfo(name = "is_done")
    public boolean isDone;

    @ColumnInfo(name = "task_id")
    public long taskId;

    @Ignore
    public Subtask(String subtaskTitle, boolean isDone) {
        this.subtaskTitle = subtaskTitle;
        this.isDone = isDone;
    }

    public Subtask(String subtaskTitle, boolean isDone, long taskId) {
        this.subtaskTitle = subtaskTitle;
        this.isDone = isDone;
        this.taskId = taskId;
    }
}
