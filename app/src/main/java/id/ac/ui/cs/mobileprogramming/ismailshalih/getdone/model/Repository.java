package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class Repository {
    private TaskDao taskDao;
    private SubtaskDao subtaskDao;
    private CategoryDao categoryDao;
    private TaskCategoryDao taskCategoryDao;

    private LiveData<List<Task>> allTasks;
    private LiveData<List<Subtask>> allSubtask;
    private LiveData<List<Category>> allCategory;
    private LiveData<List<TaskCategory>> allTaskCategory;

    public Repository(Application application) {

        TaskDatabase database = TaskDatabase.getInstance(application);

        taskDao = database.taskDao();
        subtaskDao = database.subtaskDao();
        categoryDao = database.categoryDao();
        taskCategoryDao = database.taskCategoryDao();

        allTasks = taskDao.getAllTasks();
        allSubtask = subtaskDao.getAllSubtasks();
        allCategory = categoryDao.getAllCategories();
        allTaskCategory = taskCategoryDao.getAllTaskCategory();
    }

    public LiveData<List<Task>> getAllTasks() {
        return allTasks;
    }

    public LiveData<List<Category>> getAllCategories() {
        return allCategory;
    }
    
    public LiveData<List<Subtask>> getAllSubtask() {
        return allSubtask;
    }

    public LiveData<List<TaskCategory>> getAllTaskCategory() {
        return allTaskCategory;
    }

    public long insert(Task task) {
        long taskId = 0;
        AsyncTask<Task, Void, Long> execute = new InsertTaskAsyncTask(taskDao).execute(task);
        try {
            taskId = execute.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return taskId;
    }

    public void update(Task task) {
        new UpdateTaskAsyncTask(taskDao).execute(task);
    }

    public void delete(Task task) {
        new DeleteTaskAsyncTask(taskDao).execute(task);
    }

    public void deleteAllTasks() {
        new DeleteAllTasksAsyncTask(taskDao).execute();
    }

    private static class InsertTaskAsyncTask extends AsyncTask<Task, Void, Long> {
        private TaskDao taskDao;

        private InsertTaskAsyncTask(TaskDao taskDao) {
            this.taskDao = taskDao;
        }

        @Override
        protected Long doInBackground(Task... tasks) {
            return taskDao.insert(tasks[0]);
        }
    }

    private static class UpdateTaskAsyncTask extends AsyncTask<Task, Void, Void> {
        private TaskDao taskDao;

        private UpdateTaskAsyncTask(TaskDao taskDao) {
            this.taskDao = taskDao;
        }

        @Override
        protected Void doInBackground(Task... tasks) {
            taskDao.update(tasks[0]);
            return null;
        }
    }

    private static class DeleteTaskAsyncTask extends AsyncTask<Task, Void, Void> {
        private TaskDao taskDao;

        private DeleteTaskAsyncTask(TaskDao taskDao) {
            this.taskDao = taskDao;
        }

        @Override
        protected Void doInBackground(Task... tasks) {
            taskDao.delete(tasks[0]);
            return null;
        }
    }

    private static class DeleteAllTasksAsyncTask extends AsyncTask<Void, Void, Void> {
        private TaskDao taskDao;

        private DeleteAllTasksAsyncTask(TaskDao taskDao) {
            this.taskDao = taskDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            taskDao.deleteAllTasks();
            return null;
        }
    }

    public void insert(Subtask subtask) {
        new InsertSubtaskAsyncTask(subtaskDao).execute(subtask);
    }

    public void update(Subtask subtask) {
        new UpdateSubtaskAsyncTask(subtaskDao).execute(subtask);
    }

    public void delete(Subtask subtask) {
        new DeleteSubtaskAsyncTask(subtaskDao).execute(subtask);
    }

    public void deleteAllSubtask(Subtask subtask) {
        new DeleteAllSubtasksAsyncTask(subtaskDao).execute();
    }

    public LiveData<List<Subtask>> getSubtasksByTaskId(long taskId) {
        LiveData<List<Subtask>> subtasks = null;
        AsyncTask execute = new getSubtasksByTaskIdAsyncTask(subtaskDao).execute(taskId);
        try {
            subtasks = (LiveData<List<Subtask>>) execute.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return subtasks;
    }
    
    private static class InsertSubtaskAsyncTask extends AsyncTask<Subtask, Void, Void> {
        private SubtaskDao subtaskDao;
        
        InsertSubtaskAsyncTask(SubtaskDao subtaskDao) {
            this.subtaskDao = subtaskDao;
        }

        @Override
        protected Void doInBackground(Subtask... subtasks) {
            subtaskDao.insert(subtasks[0]);
            return null;
        }
    }

    private static class UpdateSubtaskAsyncTask extends AsyncTask<Subtask, Void, Void> {
        private SubtaskDao subtaskDao;

        UpdateSubtaskAsyncTask(SubtaskDao subtaskDao) {
            this.subtaskDao = subtaskDao;
        }

        @Override
        protected Void doInBackground(Subtask... subtasks) {
            subtaskDao.update(subtasks[0]);
            return null;
        }
    }

    private static class DeleteSubtaskAsyncTask extends AsyncTask<Subtask, Void, Void> {
        private SubtaskDao subtaskDao;

        DeleteSubtaskAsyncTask(SubtaskDao subtaskDao) {
            this.subtaskDao = subtaskDao;
        }

        @Override
        protected Void doInBackground(Subtask... subtasks) {
            subtaskDao.delete(subtasks[0]);
            return null;
        }
    }

    private static class DeleteAllSubtasksAsyncTask extends AsyncTask<Void, Void, Void> {
        private SubtaskDao subtaskDao;

        DeleteAllSubtasksAsyncTask(SubtaskDao subtaskDao) {
            this.subtaskDao = subtaskDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            subtaskDao.deleteAllSubtasks();
            return null;
        }
    }

    private static class getSubtasksByTaskIdAsyncTask extends AsyncTask<Long, Void, LiveData<List<Subtask>>> {
        SubtaskDao subtaskDao;

        getSubtasksByTaskIdAsyncTask(SubtaskDao subtaskDao){
            this.subtaskDao = subtaskDao;
        }

        @Override
        protected LiveData<List<Subtask>> doInBackground(Long... longs) {
            return subtaskDao.getSubtasksByTaskId(longs[0]);
        }
    }

    public void insert(Category category) {
        new InsertCategoryAsyncTask(categoryDao).execute(category);
    }

    public void update(Category category) {
        new UpdateCategoryAsyncTask(categoryDao).execute(category);
    }

    public void delete(Category category) {
        new DeleteCategoryAsyncTask(categoryDao).execute(category);
    }

    public void deleteAllCategories() {
        new DeleteAllCategoriesAsyncTask(categoryDao).execute();
    }

    public Category getCategoryByName(String name) {
        Category category = null;
        AsyncTask execute = new GetCategoryByNameAsyncTask(categoryDao).execute(name);
        try {
            category = (Category) execute.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return category;
    }

    public LiveData<List<String>> getCategoryNames() {
        LiveData<List<String>> categoryNames = null;
        AsyncTask execute = new GetCategoryNamesAsyncTask(categoryDao).execute();
        try {
            categoryNames = (LiveData<List<String>>) execute.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return categoryNames;
    }

    private static class InsertCategoryAsyncTask extends AsyncTask<Category, Void, Void> {
        private CategoryDao categoryDao;

        InsertCategoryAsyncTask(CategoryDao categoryDao) {
            this.categoryDao = categoryDao;
        }

        @Override
        protected Void doInBackground(Category... categories) {
            categoryDao.insert(categories[0]);
            return null;
        }
    }

    private static class UpdateCategoryAsyncTask extends AsyncTask<Category, Void, Void> {
        private CategoryDao categoryDao;

        UpdateCategoryAsyncTask(CategoryDao categoryDao) {
            this.categoryDao = categoryDao;
        }

        @Override
        protected Void doInBackground(Category... categories) {
            categoryDao.update(categories[0]);
            return null;
        }
    }

    private static class DeleteCategoryAsyncTask extends AsyncTask<Category, Void, Void> {
        private CategoryDao categoryDao;

        DeleteCategoryAsyncTask(CategoryDao categoryDao) {
            this.categoryDao = categoryDao;
        }

        @Override
        protected Void doInBackground(Category... categories) {
            categoryDao.delete(categories[0]);
            return null;
        }
    }

    private static class DeleteAllCategoriesAsyncTask extends AsyncTask<Void, Void, Void> {
        private CategoryDao categoryDao;

        DeleteAllCategoriesAsyncTask(CategoryDao categoryDao) {
            this.categoryDao = categoryDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            categoryDao.deleteAllCategories();
            return null;
        }
    }

    private static class GetCategoryByNameAsyncTask extends AsyncTask<String, Void, Category> {
        private CategoryDao categoryDao;
        public GetCategoryByNameAsyncTask(CategoryDao categoryDao) {
            this.categoryDao = categoryDao;
        }

        @Override
        protected Category doInBackground(String... strings) {
            return categoryDao.getCategoryByName(strings[0]);
        }
    }

    private static class GetCategoryNamesAsyncTask extends AsyncTask<Void,Void, LiveData<List<String>>> {
        private CategoryDao categoryDao;
        public GetCategoryNamesAsyncTask(CategoryDao categoryDao) {
            this.categoryDao = categoryDao;
        }

        @Override
        protected LiveData<List<String>> doInBackground(Void... voids) {
            return categoryDao.getAllCategoryNames();
        }
    }
}
