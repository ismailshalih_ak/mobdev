package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Category;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Repository;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Subtask;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Task;


public class ViewTaskDetailViewModel extends AndroidViewModel {
    private Task task;
    private Category category;
    private LiveData<List<Subtask>> subtasks;
    private Repository repository;

    public ViewTaskDetailViewModel(@NonNull Application application) {
        super(application);
        repository = new Repository(application);
    }

    public void setTaskCategorySubtaskByTask(Task task) {
        this.task = task;
        this.category = repository.getCategoryByName(task.categoryName);
        this.subtasks = repository.getSubtasksByTaskId(task.id);
    }

    public void deleteTask() {
        repository.delete(this.task);
    }

    public Task getTask() {
        return task;
    }

    public Category getCategory() {
        return category;
    }

    public LiveData<List<Subtask>> getSubtasks() {
        return subtasks;
    }
}
