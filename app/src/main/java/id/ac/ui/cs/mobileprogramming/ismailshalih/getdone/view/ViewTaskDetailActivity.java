package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.R;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Subtask;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Task;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.viewmodel.ViewTaskDetailViewModel;

public class ViewTaskDetailActivity extends AppCompatActivity {
    public static final int EDIT_TASK_REQUEST = 2;
    private ViewTaskDetailViewModel viewTaskDetailViewModel;
    private Task task;
    private LinearLayout layout;
    private TextView textTitle;
    private TextView textDueDate;
    private TextView textDueTime;
    private TextView textCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_task_detail);

        textTitle = findViewById(R.id.text_title);
        textDueDate = findViewById(R.id.text_due_date);
        textDueTime = findViewById(R.id.text_due_time);
        textCategory = findViewById(R.id.text_category);
        layout = findViewById(R.id.task_detail_layout);

        Intent intent = getIntent();
        this.task = (Task) intent.getSerializableExtra("Task");

        viewTaskDetailViewModel = ViewModelProviders.of(this).get(ViewTaskDetailViewModel.class);
        viewTaskDetailViewModel.setTaskCategorySubtaskByTask(this.task);

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(Long.parseLong(viewTaskDetailViewModel.getTask().due + "000"));

        textTitle.setText(viewTaskDetailViewModel.getTask().title);
        textDueDate.setText(DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime()));
        textDueTime.setText(DateFormat.getTimeInstance(DateFormat.SHORT).format(c.getTime()));
        textCategory.setText(getString(R.string.category) + viewTaskDetailViewModel.getCategory().name);

        viewTaskDetailViewModel.getSubtasks().observe(this, new Observer<List<Subtask>>() {
            @Override
            public void onChanged(List<Subtask> subtasks) {
                if(subtasks.size() > 0) {
                    for (Subtask subtask : subtasks) {
                        TextView subtasktv = new TextView(ViewTaskDetailActivity.this);
                        subtasktv.setText(subtask.subtaskTitle);
                        subtasktv.setTextSize(16);
                        subtasktv.setPadding(40, 8, 8, 8);
                        layout.addView(subtasktv);
                    }
                }
            }
        });

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        setTitle("View Task Detail");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.view_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_task:
                Intent intent = new Intent(this, AddEditTaskActivity.class);
                intent.putExtra("Task", this.task);
                startActivityForResult(intent, EDIT_TASK_REQUEST);
                return true;
            case R.id.delete_task:
                viewTaskDetailViewModel.deleteTask();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == EDIT_TASK_REQUEST && resultCode == RESULT_OK) {
            Toast.makeText(this, "Task saved", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Task not saved", Toast.LENGTH_SHORT).show();
        }
    }
}
