package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface SubtaskDao {

    @Insert
    long insert(Subtask subtask);

    @Update
    void update(Subtask subtask);

    @Delete
    void delete (Subtask subtask);

    @Query("DELETE FROM subtask_table")
    void deleteAllSubtasks();

    @Query("SELECT * FROM subtask_table")
    LiveData<List<Subtask>> getAllSubtasks();

    @Query("SELECT * FROM subtask_table WHERE task_id = :taskId")
    LiveData<List<Subtask>> getSubtasksByTaskId(long taskId);
}
