package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Task;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.Repository;
import id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model.TaskCategory;

public class TaskListViewModel extends AndroidViewModel {
    private Repository repository;
    private LiveData<List<TaskCategory>> allTasks;

    public TaskListViewModel(@NonNull Application application){
        super(application);
        repository = new Repository(application);
        allTasks = repository.getAllTaskCategory();

    }

    public LiveData<List<TaskCategory>> getAllTasks() {
        return allTasks;
    }

}
