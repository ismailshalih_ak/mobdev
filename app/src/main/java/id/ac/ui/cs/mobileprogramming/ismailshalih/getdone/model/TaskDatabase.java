package id.ac.ui.cs.mobileprogramming.ismailshalih.getdone.model;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import android.content.Context;
import android.os.AsyncTask;

@Database(entities = {Task.class, Subtask.class, Category.class}, version = 8)
public abstract class TaskDatabase extends RoomDatabase {

    private static TaskDatabase instance;

    public abstract TaskDao taskDao();
    public abstract SubtaskDao subtaskDao();
    public abstract CategoryDao categoryDao();
    public abstract TaskCategoryDao taskCategoryDao();

    public static synchronized TaskDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    TaskDatabase.class, "task_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private TaskDao taskDao;
        private SubtaskDao subtaskDao;
        private CategoryDao categoryDao;
        private long exampleTaskId;

        private PopulateDbAsyncTask(TaskDatabase db) {
            taskDao = db.taskDao();
            subtaskDao = db.subtaskDao();
            categoryDao = db.categoryDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Category work = new Category("Work", "#00BCD4");
            Category personal = new Category("Personal", "#FF9800");
            categoryDao.insert(work);
            categoryDao.insert(personal);
            categoryDao.insert(new Category("Other", "#9E9E9E"));
            exampleTaskId = taskDao.insert(new Task("Example Task","1574248648", false, "Personal"));
            taskDao.insert(new Task("Another Example Task","1574848648", false, "Work"));
            subtaskDao.insert(new Subtask("Completed Subtask", true, (int) exampleTaskId));
            subtaskDao.insert(new Subtask("Uncompleted Subtask", false, (int) exampleTaskId));
            return null;
        }
    }
}
